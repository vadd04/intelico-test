/// <reference types="cypress" />

context('Form', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })


    it('.type() - type into a DOM element', () => {

        cy.get('[data-test="searchSelect"]')
            .click()
            .parent()
            .contains('Olga')
            .click()

        cy.get('[data-test="searchSelect"]')
            .should('have.text', 'Olga')

        cy.get('[ data-test="avatarSelect"]')
            .click()
            .contains('Nikita')
            .click()

        cy.get('[ data-test="avatarSelect"]')
            .should('have.text', 'Nikita')

        cy.get('[ name="age"]')
            .type('23')
            .should('have.value', '23')

        cy.get('[ name="gender"]')
            .type('male')
            .should('have.value', 'male')

        cy.get('[ name="country"]')
            .type('belarus')
            .should('have.value', 'belarus')

    })
})