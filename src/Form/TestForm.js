import React, {useState} from "react";

import s from "./TestForm.module.scss"
import SearchSelect from "../components/SearchSelect/SearchSelect";
import SelectWithAvatar from "../components/SelectWithAvatar/SelectWithAvatar";


const TestForm = props => {
    const {buyers, users} = props.data

    const initValues = {
        buyer: null,
        user: {
            name: "All users",
            photo: null
        },
        age: '',
        gender: '',
        country: '',
        city: '',
        street: '',
    }


    const [values, setValues] = useState(initValues);

    const handleSubmit = e => {
        e.preventDefault()
        console.log('FILTRED:', values)
    }

    const resetForm = (e) => {
        e.preventDefault()
        setValues(initValues)
    }

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        })
    }


    return (
        <form className={s.form} onSubmit={handleSubmit}>

            <SearchSelect
                buyers={buyers}
                buyer={values.buyer}
                setValues={setValues}
                values={values}/>

            <SelectWithAvatar
                users={users}
                user={values.user}
                setValues={setValues}
                values={values}/>

            <input
                name="age"
                type="number"
                placeholder="Age"
                value={values.age}
                onChange={handleChange}/>

            <input
                name="gender"
                type="text"
                placeholder="Gender"
                value={values.gender}
                onChange={handleChange}/>

            <input
                name="country"
                type="text"
                placeholder="Country"
                value={values.country}
                onChange={handleChange}/>

            <input
                name="city"
                type="text"
                placeholder="city"
                value={values.city}
                onChange={handleChange}/>

            <input
                name="street"
                type="text"
                placeholder="street"
                value={values.street}
                onChange={handleChange}/>

            <div className={s.buttons}>
                <button className={s.resetButton} onClick={resetForm}>Reset</button>
                <button className={s.filterButton}>Filter</button>
            </div>


        </form>
    )
}

export default TestForm
