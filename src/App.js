import React from 'react';
import TestForm from "./Form/TestForm";


function App() {
    const data = {
        buyers: [
            "TestUser1",
            "Vadim",
            "Olga",
            "Petr",
            "Semen",
            "Jora",
            "Eugene",
            "Andrey",
            "Alex",
            "Varvara",
            "Vasya",
            "Gena",
            "TestUser3",
            "TestUser2",
            "Mooy",
            "Travka"
        ],
        users: [
            {
                name: "All users",
                photo: null
            },

            {
                name: "Vadim",
                photo: "https://cdn.pixabay.com/photo/2016/11/18/23/38/child-1837375__340.png"
            },

            {
                name: "Nikita",
                photo: "https://i.ya-webdesign.com/images/boy-avatar-png-7.png"
            },

            {
                name: "Kirill",
                photo: "https://cdn2.iconfinder.com/data/icons/people-flat-design/64/Man-Person-People-Avatar-User-Happy-512.png"
            }]
    }

    return <TestForm data={data}/>
}

export default App;
