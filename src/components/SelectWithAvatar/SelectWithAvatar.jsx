import React, {useEffect, useRef, useState} from "react";
import cx from "classnames";
import s from "./SelectWithAvatar.module.scss"


const SelectWithAvatar = (
    {
        user,
        users,
        setValues,
        values
    }
) => {
    const [isSelectVisible, setSelectVisible] = useState(false)

    const ref = useRef(null);

    const handleClickOutside = event => {
        if (ref.current && !ref.current.contains(event.target)) {
            setSelectVisible(false)
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleClickOutside, true);
        return () => {
            document.removeEventListener("click", handleClickOutside, true);
        };
    });

    const handleChangeSelectorVisible = () => {
        setSelectVisible(!isSelectVisible)
    }

    const handleChangeSelectorValue = (item) => {
        setValues({
                ...values,
                user: {
                    name: item.name,
                    photo: item.photo
                }
            }
        )
        handleChangeSelectorVisible()
    }


    return (
        <div className={s.select} onClick={handleChangeSelectorVisible} data-test="avatarSelect" ref={ref}>
            <div className={cx(s.selectedValue,
                {[s.active]: isSelectVisible}
            )}>
                {user.photo && <div className={s.selectedAvatar} style={{backgroundImage: `url(${user.photo})`}}></div>}
                {user.name}
            </div>

            {isSelectVisible && <div className={s.dropdown}>
                <ul className={s.options}>
                    {users.map((item, index) =>
                        <li className={s.option}
                            data-testid="toggle"
                            onClick={() => handleChangeSelectorValue(item)}
                            key={index}
                        >
                            {item.photo &&
                            <div className={s.avatar} style={{backgroundImage: `url(${item.photo})`}}></div>}
                            {item.name}
                        </li>)}
                </ul>
            </div>}
        </div>
    )
}

export default SelectWithAvatar

