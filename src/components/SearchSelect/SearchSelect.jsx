import React, {useState, useEffect, useRef} from "react";
import cx from 'classnames'
import s from "./SearchSelect.module.scss"


const SearchSelect = (
    {
        buyers,
        buyer,
        setValues,
        values
    }
) => {
    const [isSelectVisible, setSelectVisible] = useState(false);
    const [filtredBuyers, setFiltredBuyers] = useState(buyers);

    const ref = useRef(null);

    const handleClickOutside = event => {
        if (ref.current && !ref.current.contains(event.target)) {
            setSelectVisible(false)
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleClickOutside, true);
        return () => {
            document.removeEventListener("click", handleClickOutside, true);
        };
    });


    const handleChangeSelectorVisible = () => {
        setSelectVisible(!isSelectVisible)
    };

    const handleChangeSelectorValue = (item) => {
        setValues({
            ...values,
            buyer: item,
        })
        setFiltredBuyers(buyers)
        handleChangeSelectorVisible()
    }

    const dataSearch = (e) => {
        const value = e.target.value.toLowerCase()
        const filtredArray = buyers.filter(item => item.toLowerCase().includes(value))
        setFiltredBuyers(filtredArray)
    }

    return (
        <div className={s.select} ref={ref}>
            <div
                className={cx(s.selectedOption,
                    {
                        [s.active]: isSelectVisible,
                        [s.empty]: !buyer
                    }
                )}
                data-test="searchSelect"
                onClick={handleChangeSelectorVisible}
            >
                {buyer ? buyer : 'Select buyer...'}
            </div>

            {isSelectVisible &&
            <div className={s.dropdown}>
                <input data-test="searchInput" onChange={dataSearch} className={s.input} type="text"/>

                <ul className={s.options}>
                    {filtredBuyers.map((item, index) =>
                        <li className={s.option}
                            onClick={() => handleChangeSelectorValue(item)}
                            key={index}>{item}</li>
                    )}
                </ul>
            </div>
            }
        </div>

    )
}

export default SearchSelect

