import React from "react";
import { render } from '@testing-library/react'

import SelectWithAvatar from "../SelectWithAvatar/SelectWithAvatar";
import SearchSelect from "../SearchSelect/SearchSelect";

const buyers = ['vadim', 'kirill']
const user = {name: 'Vadim', photo: null}


it('SelectWithAvatar rendered correctly',  () => {
    const {container} = render(<SelectWithAvatar user={user}/>)
    expect(container.firstChild).toMatchSnapshot()
})


it('SearchSelect rendered correctly', () => {
    const {container} = render(<SearchSelect bueyrs={buyers}/>)
    expect(container.firstChild).toMatchSnapshot()
})