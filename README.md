This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### npm run start


For testing used cypress.io
For starting test: 
You should start application on localhost:3000

Commands: 
For cypress run:
### cy:run 

For cypress open: 
### cy:open